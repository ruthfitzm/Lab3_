package ie.ucd.luggage;
import java.util.*;


public class Main {
	
	
	public static void main(String [] args) {
		//List<Luggage>  myLuggage = new ArrayList<Luggage>();							// Making a container for all the luggage
		Luggage mySuitcase = new Suitcase(20);
		SafeLuggage mysafeluggage = new SafeLuggage(12,"1234");
		
		
		
		//////////////////////////////////// Testing a normal suitcase ////////////////////////////////////
		
		System.out.println(" ---------------- Normal Suitcase ----------------");
		
		
		// First need to create the objects I want to add 
		
		
		Item Bomb = new Bomb();
		Item Pen = new Pen();
		Item Laptop = new Laptop();
		Item DesignerPen = new DesignerPen("Versace");
		
		// Now add them into my suitcase
		System.out.println("\nAdding items ...\n");
		mySuitcase.add(Bomb);
		mySuitcase.add(DesignerPen);
		mySuitcase.add(Pen);
		mySuitcase.add(Laptop);
		mySuitcase.add(DesignerPen); 
		mySuitcase.add(Bomb);
		mySuitcase.add(DesignerPen); 
		System.out.println("Checking if the items fit in the bag ...\n");
		
		// Finding the contents of the suitcase 
		List<Item> contents = mySuitcase.getContents();
		

		
		
		//Check if the suitcase has anything in it 
		if (contents != null && !contents.isEmpty()) {
			int sizeOfList=contents.size();
			
			System.out.println(" ----- Adding "+sizeOfList+" Items to luggage");

			List<Integer> list = new ArrayList<Integer>();
			
			// Update user on what is happening
			System.out.println(" ----- Checking if any dangerous items are found in the bag");
			
			// So we have a full suitcase, need to check each item to see if there are any dangerous items.
			
			for (Item c: contents) {
				boolean answer = c.isDangerous();
				if (answer) {
					System.out.println(" ----- A dangerous item has been found in your bag\n\n\n");
					list.add(1);
				
				}
				else {
					list.add(0);
				}

			}
			
			int sum = list.stream().mapToInt(Integer::intValue).sum();
			
			if (sum==0) {
				System.out.println(" ----- No Dangerous items were found in your bag\n\n\n");
				
			}
			
		}
		else {
			System.out.println(" ----- Your luggage is empty. There is nothing to check\n\n\n");
		}


		//////////////////////////////////// Testing a safe suitcase ////////////////////////////////////
		
		System.out.println(" ---------------- Safe Suitcase ----------------");
		
		// First I need to try add/ remove items to the bag 
		System.out.println("\nAdding items ...\n");
		mysafeluggage.add(Bomb);			// Testing removing a pen from safe luggage. Password required
		mysafeluggage.add(DesignerPen);
		//mysafeluggage.add(Laptop);
		//mysafeluggage.add(Pen);
		System.out.println("Checking if the items fit in the bag ...\n");
		
			
		
		// Finding the contents of the suitcase 
		List<Item> safeContents = mysafeluggage.getContents();
		
		//Check if the suitcase has anything in it 
		if (safeContents != null && !safeContents.isEmpty()) {
			mysafeluggage.removeItem(0);							// UNCOMMENT THE START OF THIS LINE TO TEST REMOVING AN ITEM!!!
			int sizeOfCont=safeContents.size();						// Need to check a second time if the bag is empty. The user might have removed the only item that was added. 
			if (sizeOfCont !=0) {
			
			
				System.out.println(" ----- Adding "+sizeOfCont+" Items to safe luggage");

				List<Integer> safelist = new ArrayList<Integer>();		// Make a safelist : add a 1 if a dangerous item was found. Add a 0 when a safe item is found
			
				// Update user on what is happening
				System.out.println(" ----- Checking if any dangerous items are found in the safe bag");
			
				// So we have a full suitcase, need to check each item to see if there are any dangerous items.
			
				for (Item c: safeContents) {
					boolean answer = c.isDangerous();
					if (answer) {
						System.out.println(" ----- A dangerous item has been found in your safe bag");
						safelist.add(1);
				
					}
					else {
						safelist.add(0);
					}

				}
			
				int sum = safelist.stream().mapToInt(Integer::intValue).sum();
			
				if (sum==0) {
					System.out.println(" ----- No Dangerous items were found in your safe bag");
				
				}
			}
			else {
				System.out.println(" ----- You have removed the only item in your bag. There is nothing to check");
			}
			
		}
		else {
			System.out.println(" ----- Your safe luggage is empty. There is nothing to check");
		}
		
		
	}
	
	

	
	}
