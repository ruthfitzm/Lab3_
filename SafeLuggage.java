package ie.ucd.luggage;
import java.util.*;

public class SafeLuggage extends Luggage{
	

	private double bagWeight;
	private String rightPassword;
	static Scanner sc = new Scanner(System.in);		// Scanner object to get user input
	double maxWeight=100;
	
	public SafeLuggage(double bagWeight, String rightPassword) {
		this.bagWeight=bagWeight;
		this.rightPassword=rightPassword;
		

	}
	
	
	@Override
	public double getBagWeight() {			// getBagWeight is an abstract method - needs to be described in the subclass
		return bagWeight;					// Just return a weight that was given as input to this class
	}

	@Override
	public double getMaxWeight() {			// getMaxWeight also is abstract so needs to be described in the subclass
		return maxWeight;							// Just returning a value I set above
	}
	
	
		// In the assignment we are asked to make a safe luggage class that requires a password to 
		// add and remove items. 
		// The right password is a string and can be compared with the user input password, to decide whether
		// or not the items can be removed/added.

	
		public void add(Item item) {						
			System.out.println("**** You are trying to add an item into safe luggage. Please enter a password:");	// You want to add an item to a safe luggage. User input is taken  
			String inputtedPassword = sc.next();		// User input is saved in a string inputtedPassword1
		
			if (rightPassword.equals(inputtedPassword)) {						// Comparing the two strings
				System.out.println("Password Correct, the item will now be added.\n");		// if password is right, you are able to add items
				super.add(item);				// Super keyword to invoke the parent class add method		
			
			}
			else {								// The password is wrong - user is not able to add.
				System.out.println("Incorrect password, the item will not be added.\n");
			
			
			
			}
		
		}	
		public void removeItem(int index) {					// Similar to add method
		
			System.out.println("**** You are trying to remove an item from a safe luggage. Please enter a password:");
			String inputtedPassword= sc.next();
		
			if (rightPassword.equals(inputtedPassword)) {
				System.out.println("Password Correct, the item will now be removed.\n");
				super.removeItem(index);
				
			}
			else {
				System.out.println("Incorrect Password, the item will not be removed.\n");
			
			
			}
		
		
		} 	
		
		


}


	
