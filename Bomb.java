package ie.ucd.luggage;

public class Bomb implements Item{
	
	public Bomb() {		// default constructor
	
	}
	

	public String getType() {
		String t="Bomb";
		return t;
		
	}
	
	
	public double getWeight() {
		double weight= 10;
		return weight;
		
	}
	
	
	public boolean isDangerous() {		// Setting manually that a bomb is a dangerous item
		boolean danger=true;
		return danger;
	
	}

	
}
